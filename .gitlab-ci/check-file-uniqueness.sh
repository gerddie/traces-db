git fetch https://gitlab.freedesktop.org/gfx-ci/tracie/traces-db.git master
set +e
MODIFIED_TRACES=$(git diff --name-status FETCH_HEAD | grep ^M | grep -e \.rdc$ -e \.trace$ -e \.gfxr$ -e \.trace-dxgi$)
set -e
if [[ -n "$MODIFIED_TRACES" ]]; then
    echo "Our caching strategy depends on trace files not being modified."
    echo "The following trace files were modified in this branch:"
    echo "$MODIFIED_TRACES"
    echo "Rename them and try again."
    exit 1
fi
